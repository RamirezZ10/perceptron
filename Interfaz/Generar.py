from .Perceptron import Region
import numpy as np
import random

class Generar():
    def __init__(self):
        pass

    def crear_rangos_aleatorios(self):

        num_puntos = random.randint(10, 200)
        x1 = random.randint(0, 60)
        x2 = random.randint(0, 60)
        y1 = random.randint(0, 60)
        y2 = random.randint(0, 60)
        region1 = Region(num_puntos, x1, x2, y1, y2)

        num_puntos = random.randint(10, 200)
        #x1 = random.randint(0, 60)
        #x2 = random.randint(0, 60)
        #y1 = random.randint(0, 60)
        #y2 = random.randint(0, 60)
        region2 = Region(num_puntos, y1, y2, x1, x2)

        regiones = [region1, region2]

        return regiones

    def leer_archivo(self, archivo):
        with open(archivo, 'r') as f:
            contenido = f.read()

        regiones = []
        region_actual = None

        lineas = contenido.split('\n')

        for i, linea in enumerate(lineas):
            if linea.startswith("Region"):
                region = Region(int(lineas[i+5]), int(lineas[i+1]), int(lineas[i+2]), int(lineas[i+3]), int(lineas[i+4]))
                regiones.append(region)

        return regiones

        
if __name__ == "__main__":
    g = Generar()
    g.leer_archivo("a.txt")
