import random
import numpy as np
import matplotlib.pyplot as plt

class Perceptron:
    def __init__(self, num_caracteristicas, aprendizaje, pesos_i, pesos_f):
        self.num_caracteristicas = num_caracteristicas
        self.aprendizaje = aprendizaje
        self.pesos = np.random.uniform(pesos_i, pesos_f, num_caracteristicas)

        print(self.pesos)

        self.bias = np.random.randn()

    def activation_function(self, x):
        if x >= 0:
            return 1
        else:
            return 0
        #return 1 if x >= 0 else 0

    def predict(self, x):
        return self.activation_function(np.dot(self.pesos, x) + self.bias)

    def train(self, x_train, y_train, num_epocas):
        for epoca in range(num_epocas):
            total_error = 0
            for i, x in enumerate(x_train):
                y_pred = self.predict(x)
                error = y_train[i] - y_pred
                self.pesos += self.aprendizaje * error * x
                self.bias += self.aprendizaje * error
                total_error += abs(error)
            if epoca % 10 == 0:
                print(f"Epoca {epoca} - Total error: {total_error}")

    def evaluate(self, x_test, y_test):
        num_correct = 0
        for i, x in enumerate(x_test):
            y_pred = self.predict(x)
            if y_pred == y_test[i]:
                num_correct += 1
        accuracy = num_correct / len(y_test)
        print(f"Accuracy: {accuracy}")

    

class Main:
    def __init__(self, region_A, region_B, num_caracteristicas, aprendizaje, pesos_i, pesos_f):
        self.region_A = region_A
        self.region_B = region_B

        self.generarConjuntos()
        self.crearPerceptron(num_caracteristicas, aprendizaje, pesos_i, pesos_f)
        #self.plotear()

    def generarConjuntos(self):
        self.x_train = np.concatenate([self.region_A.class_1, self.region_B.class_1])
        self.y_train = np.concatenate([np.ones(self.region_A.num_puntos), np.zeros(self.region_B.num_puntos)])

    def crearPerceptron(self, num_caracteristicas, aprendizaje, pesos_i, pesos_f):
        self.perceptron = Perceptron(num_caracteristicas, aprendizaje, pesos_i, pesos_f)
        self.perceptron.train(self.x_train, self.y_train, num_epocas = 100)

    def plotear(self):
        plt.scatter(self.region_A.class_1[:,0], self.region_A.class_1[:,1], c="red")
        plt.scatter(self.region_B.class_1[:,0], self.region_B.class_1[:,1], c="blue")
        x_min, x_max = plt.xlim()
        y_min, y_max = plt.ylim()
        xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.1), np.arange(y_min, y_max, 0.1))
        Z = np.array([self.perceptron.predict([xx1, yy1]) for xx1, yy1 in np.c_[xx.ravel(), yy.ravel()]])
        Z = Z.reshape(xx.shape)
        plt.contour(xx, yy, Z, colors=["green"])
        plt.savefig("Interfaz/static/plot.png")
        plt.clf()
        #plt.show()

    def predecir(self, _region_A, _region_B):
        plt.scatter(_region_A.class_1[:,0], _region_A.class_1[:,1], c="red")
        plt.scatter(_region_B.class_1[:,0], _region_B.class_1[:,1], c="blue")
        x_min, x_max = plt.xlim()
        y_min, y_max = plt.ylim()

        X_test = np.concatenate(_region_A, _region_B)

        x_min, x_max = np.min(X_test[:, 0]), np.max(X_test[:, 0])
        y_min, y_max = np.min(X_test[:, 1]), np.max(X_test[:, 1])

        # Generar una malla de puntos
        xx, yy = np.meshgrid(np.linspace(x_min, x_max, 100), np.linspace(y_min, y_max, 100))

        # Calcular las predicciones de la línea de decisión
        Z = perceptron.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        # Plotear los puntos de prueba y la línea de decisión
        plt.contourf(xx, yy, Z, alpha=0.3, cmap='viridis')
        plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap='viridis')

        #xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.1), np.arange(y_min, y_max, 0.1))
        #Z = np.array([self.perceptron.predict([xx1, yy1]) for xx1, yy1 in np.c_[xx.ravel(), yy.ravel()]])
        #Z = Z.reshape(xx.shape)
        #plt.contour(xx, yy, Z, colors=["green"])
        plt.savefig("Interfaz/static/prediccion.png")
        plt.clf()

    def hola(self):
        print("HOLAAAA")


class Region:
    def __init__(self, num_puntos, x_min, x_max, y_min, y_max):
        self.num_puntos = num_puntos
        self.x_max = x_max
        self.x_min = x_min
        self.y_max = y_max
        self.y_min = y_min
        self.class_1 = np.array( [(random.uniform(self.x_min, self.x_max), random.uniform(self.y_min, self.y_max)) for _ in range(self.num_puntos)] )
        #print(self.class_1)

if __name__ == "__main__":
    regionA = Region(100, -15, 0, 0, 10)
    regionB = Region(200, -50, 10, -10, 0)

    main = Main(regionA, regionB, 2, 0.3)
