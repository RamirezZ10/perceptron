from django.shortcuts import render
from .Perceptron import Region, Main
from .Generar import Generar 

import pickle, json
from django.core.cache import cache


# Create your views here.

def Index(request):

    #archivo = request.FILES.get('archivo')
    if request.method == "POST":
        if "entrenar" in request.POST:
            btn_pressed = "entrenar"
            regionA = Region(int(request.POST["n-region1"]), 
                             int(request.POST["x1-region1"]), 
                             int(request.POST["x2-region1"]), 
                             int(request.POST["y1-region1"]), 
                             int(request.POST["y2-region1"])
                             )

            regionB = Region(int(request.POST["n-region2"]), 
                             int(request.POST["x1-region2"]), 
                             int(request.POST["x2-region2"]), 
                             int(request.POST["y1-region2"]), 
                             int(request.POST["y2-region2"])
                             )

            main = Main(regionA, 
                        regionB, 
                        2, 
                        float(request.POST["coef_aprendizaje"]), 
                        float(request.POST["pesos_min"]),
                        float(request.POST["pesos_max"])
                        )

            main.plotear()
            cache.set('main', main)

        elif "reconocer" in request.POST:
            btn_pressed = "reconocer"

            if request.POST["Generar"] == "Aleatoriamente":
                g = Generar()
                regiones = g.crear_rangos_aleatorios()

            elif request.POST["Generar"] == "Archivo":
                g = Generar()
                print(request.FILES)
                archivo = request.FILES["archivo"]

                with open('archivo.txt', 'wb+') as destination:
                    for chunk in archivo.chunks():
                        destination.write(chunk)

                regiones = g.leer_archivo("archivo.txt")

            main = Main(regiones[0], 
                        regiones[1], 
                        2, 
                        0.4, 
                        0.1,
                        0.9
                        )
            main.plotear()

        #print("---------------------\n")
        #print(request.POST)
        #print("---------------------\n")


        return render(request, "Index.html", {
            "btn_pressed":btn_pressed,
        })

    else:
        return render(request, "Index.html")
